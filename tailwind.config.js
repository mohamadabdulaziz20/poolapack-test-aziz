const colors = require('tailwindcss/colors');

module.exports = {
  purge: {
    content: [
      `components/**/*.{vue,js}`,
      `layouts/**/*.vue`,
      `pages/**/*.vue`,
      `plugins/**/*.{js,ts}`,
      `nuxt.config.{js,ts}`
    ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontSize: {
        xss: '10px',
        xsxs: '8px',
      },
      borderWidth: {
        DEFAULT: '1px',
        '1.5': '1.5px',
        '3': '3px',
      },
      colors: {
        black: colors.black,
        white: colors.white,
        transparent: 'transparent',
        current: 'currentColor',
        green: {
          50: colors.emerald[50],
          100: '#edfaf3',
          200: '#dcf5e7',
          300: '#caf0db',
          400: '#b8ebcf',
          500: '#a7e7c4',
          600: '#95e2b8',
          700: '#83ddac',
          800: '#72d8a0',
          900: '#60d394'
        },
        orange: {
          50: colors.orange[50],
          100: '#fff1e3',
          200: '#fee4c6',
          300: '#fed6aa',
          400: '#fdc98e',
          500: '#fdbb71',
          600: '#fcae55',
          700: '#fca039',
          800: '#fb931c',
          900: '#fb8500'
        },
        red: {
          50: colors.rose[50],
          100: '#ffe9ec',
          200: '#ffd3d9',
          300: '#ffbdc5',
          400: '#ffa7b2',
          500: '#fe929f',
          600: '#fe7c8c',
          700: '#fe6678',
          800: '#fe5065',
          900: '#4B1412'
        },
        'yellow-primary': '#F9BE00',
        'cyan-primary': '#46D5B2',
        yellow: {
          50: colors.amber[50],
          100: '#fff7e3',
          200: '#ffefc7',
          300: '#ffe7ab',
          400: '#ffdf8f',
          500: '#ffd773',
          600: '#ffcf57',
          700: '#ffc73b',
          800: '#ffbf1f',
          900: '#ffb703'
        },
        'smoke-red': {
          50: colors.red[50],
          100: '#feedec',
          200: '#fcdbd9',
          300: '#fbc9c6',
          400: '#f9b7b3',
          500: '#f8a4a0',
          600: '#f6928d',
          700: '#f5807a',
          800: '#f36e67',
          900: '#f25c54'
        },
        'smoke-yellow': {
          50: colors.yellow[50],
          100: '#fef3ec',
          200: '#fde8d8',
          300: '#fcdcc5',
          400: '#fbd1b1',
          500: '#fbc59e',
          600: '#faba8a',
          700: '#f9ae77',
          800: '#f8a363',
          900: '#f79750'
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
