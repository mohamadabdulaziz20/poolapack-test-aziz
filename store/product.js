export const state = () => ({
  list: [],
  loadingList: false
});

export const mutations = {
  LIST(state, payload) {
    state.list = payload || [];
  },
  LOADING_LIST(state, payload) {
    state.loadingList = payload;
  },
}

export const getters = {
  getList: state => state.list,
  getLoadingList: state => state.loadingList,
}

export const actions = {
  /**
   * Get List of Product
   * Access with `store.dispatch('fetchProduct')`
   * @param commit
   * @returns {Promise<unknown>}
   */
  fetchProduct({ commit }) {
    return new Promise((resolve, reject) => {
      commit('LOADING_PRODUCT', true);
      this.$axios({
        url: "https://61cf614865c32600170c7f4c.mockapi.io/api/accreditation/institute", 
        method: 'GET',
      })
      .then(resp => {
          commit('LIST', resp.data || []);
          return resolve(resp);
        })
        .catch(e => {
          commit('LIST', []);
          return reject(e);
        })
        .finally(() =>{ 
          commit('LOADING_LIST', false);
        });
    });
  },
}
