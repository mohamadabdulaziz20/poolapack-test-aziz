export const state = () => ({
  authenticated: {
    email: "",
    password: ""
  }
});

export const mutations = {
  AUTH(state, payload) {
    state.authenticated = payload || {};
  },
}

export const getters = {
  getAuth: state => state.authenticated,
}

export const actions = {
  /**
   * Insert User
   * @param commit
   * @returns {Promise<unknown>}
   */
  InsertUser({ commit }, user) {
    commit('AUTH', user || {});
  },
}
