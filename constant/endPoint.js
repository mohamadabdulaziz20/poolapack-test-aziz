/*
 * End Poin of Product
 */
export const HOST_NAME = `htpps://poolapack.com`
const API_VERSION = `/api/v2`
export const URL = (url = ``) => `${HOST_NAME}${API_VERSION}${url}`;

export const PRODUCTS = {
  PRODUCT_LIST: (params = '') =>
    URL(`/gifts${params}`),
  PRODUCT_DETAIL: (id = '') =>
    URL(`/gift/${id}`),
  PUT_WISH_LIST: (id = '') =>
    URL(`/gifts/${id}/wishlist`),
};

export const Dummy = []